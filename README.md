# GOALS
Test out using chalice to deploy some endpoints.

This application is not really a micro service as it has multiple endpoints all doing different logic and that is antithetical to micro-services architecture which presupposes that each endpoint is one function.

However this is just to flex using chalice. Followed the instructions from the github here.
https://github.com/aws/chalice

at the bottom there a few endpoints related to leveraging AWS dynamo DB mainly because I am intollerant of document databases and my inflexible mind prefers sleek, awesome and pre-defined table schemas. Schemas are so good.