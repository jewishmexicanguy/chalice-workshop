from chalice import Chalice
from chalice import NotFoundError

app = Chalice(app_name='sogeseek')

OBJECTS = {}

@app.route('/')
def index():
    return {'hello': 'world'}

@app.route('/objects/{key}', methods=['GET','PUT'])
def myobject(key):
    request = app.current_request
    if request.method == 'PUT':
        OBJECTS[key] = request.json_body
    elif request.method == 'GET':
        try:
            return {key: OBJECTS[key]}
        except KeyError:
            raise NotFoundError(key)

@app.route('/introspect')
def introspect():
    return app.current_request.to_dict()

# import urllib parse
from urllib.parse import urlparse, parse_qs

@app.route('/parse_raw', methods=['POST'], content_types=['application/x-www-form-urlencoded'])
def parse_raw():
    """
    In this function we showcase how to parse body data sent in that is not of content type "application/json".
    By accessing the .raw_body attribute we can get the body. 
    We do this because When the type is not of type 'application/json' json_body will be None type.
    """
    parsed = parse_qs(app.current_request.raw_body.decode())
    return {'states':parsed.get('states',[])}

from chalice import Response
@app.route('/custom-response')
def custom_response():
    """
    This showcases using chalice.Response to returna a respose in something other than JSON.
    """
    return Response(body='hello world!', status_code=200, headers={'Content-Type':'text/plain'})

import json
import gzip
app.api.binary_types.append('application/json')

@app.route('/dogzip')
def dogzip():
    blob = json.dumps({'hello':'world'}).encode('utf-8')
    payload = gzip.compress(blob)
    custom_headers = {
        'Content-Type':'application/json',
        'Content-Encoding':'gzip'
    }
    return Response(body=payload, status_code=200,headers=custom_headers)

@app.route('/cors-request', methods=['POST'],cors=True)
def do_cors_request():
    """
    This endpoint will showcase how to support cors requests with chalice.
    The attribute of the decorator is modified to accept cors because by default it is set to false.
    The following behaviours are added to the AWS console for the api gateway.
        Injecting the Access-Control-Allow-Origin: * header to your responses, including all error responses you can return.
        Automatically adding an OPTIONS method to support preflighting requests.
    """
    return {
        'key1':'value1',
        'key2':'value2'
    }

import boto3
from botocore.exceptions import ClientError

S3 = boto3.client('s3', region_name='us-east-1')
BUCKET = 'sogeseek'

@app.route('/data/{key}', methods=['GET','POST'])
def s3objects(key):
    """
    simialar to the above endpoint '/objects/{key}' this endpoint will store json to an S3 bucket and retrive it from the S3 bucket
    And I now realize that S3 can act as a simple database, by trating it like a document database.
    """
    request = app.current_request
    if request.method == 'POST':
        S3.put_object(
            Bucket=BUCKET, 
            Key=key, 
            Body=json.dumps(request.json_body))
        return {"status":"ok"}
    elif request.method == 'GET':
        try:
            response = S3.get_object(Bucket=BUCKET, Key=key)
            return json.loads(response['Body'].read())
        except ClientError as e:
            raise NotFoundError(key)

"""
Just because I prefer relational databases over other kinds of lower life forms, I want to see how inserting into AWS Dynamo DB is like using chalice.
"""

@app.route('/create/principle', methods=['POST'])
def create_pricniple():
    """
    Create 
    """
    return Response(body="function not implimented",
        status_code=200,
        headers={'Content-Type':'text/plain'}
    )

@app.route('/get/principle/{id}', methods=['GET'])
def get_principle(id):
    """
    Read
    """
    return Response(body="function not implimented",
        status_code=200,
        headers={'Content-Type':'text/plain'}
    )

@app.route('/list/principles', methods=['GET'])
def list_principle():
    """
    Read Many
    """
    return Response(body="function not implimented",
        status_code=200,
        headers={'Content-Type':'text/plain'}
    )

@app.route('/edit/principle/{id}', methods=['PUT'])
def put_principle(id):
    """
    Update
    """
    return Response(body="function not implimented",
        status_code=200,
        headers={'Content-Type':'text/plain'}
    )

@app.route('/delete/principle/{id}', methods=['DELETE'])
def delete_principle(id):
    """
    Delete
    """
    return Response(body="function not implimented",
        status_code=200,
        headers={'Content-Type':'text/plain'}
    )